# Kodi Media Center language file
# Addon Name: Njoy N7 PVR Client
# Addon id: pvr.njoy
# Addon Provider: Team Kodi
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2025-01-17 23:18+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Hebrew (Israel) <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-njoy/he_il/>\n"
"Language: he_il\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 5.9.2\n"

msgctxt "Addon Summary"
msgid "Njoy N7 PVR Client"
msgstr "לקוח טלוויזיה חיה עבור Njoy N7"

msgctxt "#30000"
msgid "N7 IP"
msgstr "שם מארח או כתובת IP"

msgctxt "#30001"
msgid "N7 Port"
msgstr "פורט"
