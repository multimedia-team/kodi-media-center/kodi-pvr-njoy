# Kodi Media Center language file
# Addon Name: Njoy N7 PVR Client
# Addon id: pvr.njoy
# Addon Provider: Team Kodi
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2025-01-17 23:18+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Arabic (Saudi Arabia) <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-njoy/ar_sa/>\n"
"Language: ar_sa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 5.9.2\n"

msgctxt "Addon Summary"
msgid "Njoy N7 PVR Client"
msgstr ""

# settings labels
msgctxt "#30000"
msgid "N7 IP"
msgstr ""

msgctxt "#30001"
msgid "N7 Port"
msgstr ""
